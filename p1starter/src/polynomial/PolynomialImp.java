package polynomial;

import java.util.Iterator;
import java.util.NoSuchElementException;
import list.List;
import list.ListFactory;

public class PolynomialImp implements Polynomial {
	private String polynomial;
	private ListFactory<Term> listFactory;
	private List<Term> list;
	
	public PolynomialImp(String pol) {
		listFactory =  TermListFactory.newListFactory();
		list = listFactory.newInstance();
		polynomial = pol;
		stringToTerm();
	}
	
	public void stringToTerm() {
		String[] polySplited = polynomial.split("[+]");
		Term term;
		for(int i = 0; i < polySplited.length; i++) {
			if(!polySplited[i].contains("x")) { //exponent 0
				term = new TermImp(Double.parseDouble(polySplited[i]), 0);
			}
			else if(polySplited[i].charAt(0) == 'x' && polySplited[i].length() > 1) { //coefficient 1
				term = new TermImp(1, Integer.parseInt(polySplited[i].substring(2)));
			}
			else if(!polySplited[i].contains("^") && polySplited[i].contains("x") && polySplited[i].length() > 1) { //exponent 1
				term = new TermImp(Double.parseDouble(polySplited[i].substring(0, polySplited[i].length() - 1)), 1);
			}
			else if(polySplited[i].charAt(0) == 'x') { // Coefficient and exponent 1
				term = new TermImp(1, 1);
			}
			else {
				String[] polySplited1 = polySplited[i].split("[x][\\W]");
				term = new TermImp(Double.parseDouble(polySplited1[0]), Integer.parseInt(polySplited1[1]));
			}
			list.add(term);
		}
	}
	
	public class termIterator implements Iterator<Term> {
		private int current = 0; // index of the character of theString to be returned by next next()
		private boolean canRemove;
		
		@Override
		public boolean hasNext() {
			return current < list.size();
		}
		
		@Override
		public Term next() {
			if (!hasNext()) throw new NoSuchElementException("No more elements...");
			canRemove = true;
			return list.get(current++);
		}
		
		@Override
		public void remove() throws IllegalStateException {
			if (!canRemove) throw new IllegalStateException("Invalid to remove.");
			list.remove(--current);
			canRemove = false;
		}
	}

	@Override
	public Iterator<Term> iterator() {
		return new termIterator();
	}

	@Override
	public Polynomial add(Polynomial P2) {
		int pos = 0;
		Iterator<Term> iter1 = iterator();
		while(iter1.hasNext()) {
			Term term1 = iter1.next();
			Iterator<Term> iter2 = P2.iterator();
			while(iter2.hasNext()) {
				Term term2 = iter2.next();
				if(term1.getExponent() == term2.getExponent()) {
					((TermImp) term1).setCoefficient(term1.getCoefficient() + term2.getCoefficient());
					if(term1.getCoefficient() == 0) {
						iter1.remove();
					}
					iter2.remove();
				}
				if(term2.getExponent() > term1.getExponent()) {
					list.add(pos, term2);
					pos++;
					iter1.next();
					iter2.remove();
				}
			}
			pos++;
		}
		return this;
	}

	@Override
	public Polynomial subtract(Polynomial P2) {
		int pos = 0;
		Iterator<Term> iter1 = iterator();
		if(this.equals(P2)) {
			while(iter1.hasNext()) {
				iter1.next();
				iter1.remove();
			}
			Term term = new TermImp(0, 0);
			list.add(term);
		}
		else {
			while(iter1.hasNext()) {
				Term term1 = iter1.next();
				Iterator<Term> iter2 = P2.iterator();
				while(iter2.hasNext()) {
					Term term2 = iter2.next();
					if(term1.getExponent() == term2.getExponent()) {
						((TermImp) term1).setCoefficient(term1.getCoefficient() - term2.getCoefficient());
						if(term1.getCoefficient() == 0) {
							iter1.remove();
						}
						iter2.remove();
					}
					if(term2.getExponent() > term1.getExponent()) {
						((TermImp)term2).setCoefficient(-term2.getCoefficient());
						list.add(pos, term2);
						pos++;
						iter1.next();
						iter2.remove();
					}
				}
				pos++;
			}
		}
		return this;
	}

	@Override
	public Polynomial multiply(Polynomial P2) {
		if(P2.toString().equals("0")) {
			Iterator<Term> iter = iterator();
			((TermImp) iter.next()).setCoefAndExp(0, 0);
			while(iter.hasNext()) {
				iter.next();
				iter.remove();
			}
		}
		else {
			Polynomial clonePoly = new PolynomialImp(polynomial);
			list.clear();
			for(Term term1 : clonePoly) {
				for(Term term2 : P2) {
					Term term = new TermImp(term1.getCoefficient() * term2.getCoefficient(),term1.getExponent() + term2.getExponent());
					list.add(term);
				}
			}
			sort();
			for(Term term3 : this) {
				Iterator<Term> iter4 = iterator();
				while(iter4.hasNext()) {
					Term term4  = iter4.next();
					if(term3.getExponent() == term4.getExponent() && term3 != term4) {
						((TermImp)term3).setCoefficient(term3.getCoefficient() + term4.getCoefficient());
						iter4.remove();
					}
				}
			}
		}
		return this;
	}

	@Override
	public Polynomial multiply(double c) {
		Iterator<Term> iter = iterator();
		if(c == 0) {
			((TermImp) iter.next()).setCoefAndExp(0, 0);
			while(iter.hasNext()) {
				iter.next();
				iter.remove();
			}
		}
		else {
			for(Term e : this){
				((TermImp) e).setCoefficient(e.getCoefficient() * c);
			}
		}
		return this;
	}

	@Override
	public Polynomial derivative() {
		Iterator<Term> iter = iterator();
		while(iter.hasNext()) {
			Term e = iter.next();
			if(e.getExponent() != 0) {
				((TermImp) e).setCoefAndExp((e.getCoefficient() * e.getExponent()), e.getExponent() - 1);
			}
			else {
				iter.remove();
			}
		}
		return this;
	}

	@Override
	public Polynomial indefiniteIntegral() {
		for(Term term : this) {
			((TermImp)term).setCoefAndExp((term.getCoefficient() / (term.getExponent() + 1)), term.getExponent() + 1);
		}
		Term constant = new TermImp(1, 0);
		list.add(constant);
		return this;
	}

	@Override
	public double definiteIntegral(double a, double b) {
		indefiniteIntegral();
		return evaluate(b) - evaluate(a);
	}

	@Override
	public int degree() {
		Iterator<Term> iter = iterator();
		return iter.next().getExponent();
	}

	@Override
	public double evaluate(double x) {
		double sum = 0;
		for(Term term : this) {
			sum += term.evaluate(x);
		}
		return sum;
	}

	@Override
	public boolean equals(Polynomial P) {
		if(P.toString().equals(toString())) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		String string = "";
		for(Term term : this) {
			string += "+";
			if(term.getCoefficient() != 0 && term.getExponent() == 0) { // constant
				string += term.getCoefficient();
			}
			else if(term.getCoefficient() == 1 && term.getExponent() > 1) {// coef 1
				string += "x^" + term.getExponent();
			}
			else if(term.getCoefficient() == 1 && term.getExponent() == 1) { // coef y exp 1
				string += "x";
			}
			else if(term.getExponent() == 1 && term.getCoefficient() != 0) { // exp 1
				string += term.getCoefficient() + "x";
			}
			else if(term.getCoefficient() == 0 && term.getExponent() == 0) { // 0
				string += "0";
			}
			else {
				string += term.getCoefficient() + "x^" + term.getExponent();
			}
		}
		string = string.replaceAll("\\.0", ".00");
		string = string.replaceFirst("\\W", "");
		return string;
	}
	
	/**
	 * Sort the list in decreasing order of exponents using insertion sort algorithm.
	 */
    public void sort() { 
        int n = list.size();
        for (int i = 1; i < n; ++i) { 
            Term key = list.get(i); 
            int j = i - 1; 
            while (j >= 0 && list.get(j).getExponent() < key.getExponent()) { 
                list.set(j + 1, list.get(j));
                j = j - 1; 
            } 
            list.set(j + 1, key);
        } 
    } 
}