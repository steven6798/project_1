package polynomial;

public class TermImp implements Term {
	private double coefficient;
	private int exponent;
	
	public TermImp(double coe, int exp) {
		coefficient = coe;
		exponent = exp;
	}

	@Override
	public double getCoefficient() {
		return coefficient;
	}

	@Override
	public int getExponent() {
		return exponent;
	}

	@Override
	public double evaluate(double x) {
		return coefficient * (Math.pow(x, exponent));
	}
	
	/**
	 * Replace the coefficient of the term
	 * @param coef the value to replace
	 */
	public void setCoefficient(double coef) {
		coefficient = coef;
	}
	
	/**
	 * Replace the exponent of the term
	 * @param exp the value to replace
	 */
	public void setExponent(int exp) {
		exponent = exp;
	}
	
	/**
	 * Replace the coefficient of the term
	 * @param coef the value of the coefficient to replace
	 * @param exp the value of the exponent to replace
	 */
	public void setCoefAndExp(double coef, int exp) {
		coefficient = coef;
		exponent = exp;
	}
}