package list;

public class SinglyLinkedListFactory<E> implements ListFactory<E>{

	/**
	 * Creates a new SinglyLinkedList container
	 * @return the newly created container.
	 */
	public SinglyLinkedList<E> newInstance() {
		return new SinglyLinkedList<E>();
	}

}
