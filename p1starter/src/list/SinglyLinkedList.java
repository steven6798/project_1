package list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SinglyLinkedList<E> implements List<E> {
	private SNode<E> head; 
	private int size; 
	
	public SinglyLinkedList() { 
		head = new SNode<E>(); 
		size = 0; 
	}
	
	public class SinglyLinkedListIterator implements Iterator<E> {
		private SNode<E> target = head;
		private SNode<E> prev = target;
		private boolean canRemove; // true if remove is possible, false if not
		
		@Override
		public boolean hasNext() {
			return target.getNext() != null;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public E next() {
			if (!hasNext()) throw new NoSuchElementException("Iterator has completed...");
			canRemove = true;
			prev = target;
			return (E) (target = target.getNext());
		}
		
		@Override
		public void remove() {
			if (!canRemove) throw new IllegalStateException("Invalid to remove.");
			prev.setNext(target.getNext());
			cleanNode(target);
			canRemove = false;
		}
	}

	@Override
	public Iterator<E> iterator() {
		return new SinglyLinkedListIterator();
	}

	@Override
	public void add(E obj) {
		add(size, obj);
	}

	@Override
	public void add(int index, E obj) throws IndexOutOfBoundsException {
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException("add: invalid index = " + index);
		}
		SNode<E> newNode = new SNode<E>(obj);
		if(index == 0) {
			newNode.setNext(head.getNext());
			head.setNext(newNode);
		}
		else {
			SNode<E> target = head;
			for(int i = 0; i < index; i++) {
				target = target.getNext();
			}
			newNode.setNext(target.getNext());
			target.setNext(newNode);
		}
		size++;
	}

	@Override
	public boolean remove(E obj) {
		SNode<E> target = head;
		SNode<E> prev = target;
		for(int i = 0; i < size; i++) {
			target = target.getNext();
			if(target.getElement().equals(obj)) {
				prev.setNext(target.getNext());
				cleanNode(target);
				size--;
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException {
		if(isEmpty() || index < 0 || index > size) {
			throw new IndexOutOfBoundsException("remove: invalid index = " + index);
		}
		SNode<E> target = head;
		SNode<E> prev = target;
		for(int i = 0; i <= index; i++) {
			prev = target;
			target = target.getNext();
		}
		prev.setNext(target.getNext());
		cleanNode(target);
		size--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		int counter = 0;
		while(firstIndex(obj) > -1) {
			remove(firstIndex(obj));
			counter++;
		}
		return counter;
	}

	@Override
	public E get(int index) throws IndexOutOfBoundsException {
		if(isEmpty() || index < 0 || index > size) {
			throw new IndexOutOfBoundsException("remove: invalid index = " + index);
		}
		SNode<E> target = head;
		for(int i = 0; i <= index; i++) {
			target = target.getNext();
		}
		return target.getElement();
	}

	@Override
	public E set(int index, E obj) throws IndexOutOfBoundsException {
		if(isEmpty() || index < 0 || index > size) {
			throw new IndexOutOfBoundsException("remove: invalid index = " + index);
		}
		SNode<E> target = head;
		for(int i = 0; i <= index; i++) {
			target = target.getNext();
		}
		E oldElement = target.getElement();
		target.setElement(obj);
		return oldElement;
	}

	@Override
	public E first() {
		if(!isEmpty()) {
			return head.getNext().getElement();
		}
		else {
			return null;
		}
	}

	@Override
	public E last() {
		SNode<E> target = head;
		for(int i = 0; i <= size; i++) {
			target = target.getNext();
		}
		return target.getElement();
	}

	@Override
	public int firstIndex(E obj) {
		SNode<E> target = head;
		for(int i = 0; i <= size; i++) {
			target = target.getNext();
			if(obj.equals(target.getElement())) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		SNode<E> target = head;
		int pos = -1;
		for(int i = 0; i <= size; i++) {
			target = target.getNext();
			if(obj.equals(target.getElement())) {
				pos = i;
			}
		}
		return pos;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(E obj) {
		return firstIndex(obj) > -1;
	}

	@Override
	public void clear() {
		for(int i = 0; i <= size - 1; i++) {
			SNode<E> target = head.getNext();
			head.setNext(target.getNext());
			cleanNode(target);
		}
		size = 0;
	}
	
	public void cleanNode(SNode<E> node) {
		node.setElement(null);
		node.setNext(null);
	}
	
	private static class SNode<T> {
		private T element; 
		private SNode<T> next; 
		public SNode() { 
			element = null; 
			next = null; 
		}
		public SNode(T data)  { 
			this.element = data; 
			next = null; 
		}
		public T getElement() {
			return element;
		}
		public void setElement(T data) {
			this.element = data;
		}
		public SNode<T> getNext() {
			return next;
		}
		public void setNext(SNode<T> next) {
			this.next = next;
		}
	}
}