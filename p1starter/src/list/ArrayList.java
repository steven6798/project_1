package list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E> {
	private static final int INITCAP = 5;
	private E[] elements;
	private int size;
	
	@SuppressWarnings("unchecked")
	public ArrayList() { 
		elements = (E[]) new Object[INITCAP]; 
		size = 0; 
	}
	
	public class ArrayListIterator implements Iterator<E> {
		private int current; // the index of the next element for next()
		private boolean canRemove; // true if remove is possible, false if not
		
		@Override
		public boolean hasNext() {
			return current < size;
		}
		
		@Override
		public E next() {
			if(!hasNext()) throw new NoSuchElementException("Iterator has completed...");
			canRemove = true;
			return elements[current++];
		}
		
		@Override
		public void remove() {
			if(!canRemove) throw new IllegalStateException("Invalid to remove.");
			ArrayList.this.remove(--current);
			canRemove = false;
		}
	}
		
	@Override
	public Iterator<E> iterator() {
		return new ArrayListIterator();
	}

	@Override
	public void add(E obj) {
		if(size == elements.length - 1) {
			changeCapacity(size);
		}
		elements[size] = obj;
		size++;
	}

	@Override
	public void add(int index, E obj) throws IndexOutOfBoundsException {
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException("add: invalid index = " + index);
		}
		if(size == elements.length - 1) {
			changeCapacity(size);
		}
		if(elements[index] != null) {
			moveDataOnePositionTR(index, size - 1);
		}
		elements[index] = obj;
		size++;
	}

	@Override
	public boolean remove(E obj) {
		if(firstIndex(obj) > -1) {
			remove(firstIndex(obj));
			size--;
			return true;
		}
		return false;
	}

	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException {
		if(isEmpty() || index < 0 || index > size) {
			throw new IndexOutOfBoundsException("remove: invalid index = " + index);
		}
		moveDataOnePositionTL(index + 1, size);
		size--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		int counter = 0;
		while(firstIndex(obj) > -1) {
			remove(firstIndex(obj));
			counter++;
		}
		return counter;
	}

	@Override
	public E get(int index) throws IndexOutOfBoundsException {
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException("index invalid");
		}
		return elements[index]; 
	}

	@Override
	public E set(int index, E obj) throws IndexOutOfBoundsException {
		if(index < 0 || index > elements.length - 1) {
			throw new IndexOutOfBoundsException("set: invalid index = " + index);
		}
		E old = elements[index];
		elements[index] = obj;
		return old;
	}

	@Override
	public E first() {
		if(!isEmpty()) {
			return elements[0];
		}
		else {
			return null;
		}
	}

	@Override
	public E last() {
		if(!isEmpty()) {
			return elements[size - 1];
		}
		else {
			return null;
		}
	}

	@Override
	public int firstIndex(E obj) {
		for(int i = 0; i < size; i++) {
			if(obj.equals(elements[i])) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		for(int i = size; i >= 0; i--) {
			if(obj.equals(elements[i])) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(E obj) {
		return firstIndex(obj) > -1;
	}

	@Override
	public void clear() {
		for(int i = 0; i < size; i++) {
			elements[i] = null;
		}
		size = 0;
	}
	
	@SuppressWarnings("unchecked")
	private void changeCapacity(int change) { 
		int newCapacity = elements.length + change; 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i = 0; i < size; i++) { 
			newElement[i] = elements[i]; 
			elements[i] = null; 
		} 
		elements = newElement; 
	}
	
	private void moveDataOnePositionTR(int low, int sup) {
		for (int pos = sup; pos >= low; pos--)
			elements[pos+1] = elements[pos];
	}

	private void moveDataOnePositionTL(int low, int sup) {
		for (int pos = low; pos <= sup; pos++)
			elements[pos-1] = elements[pos];
	}
}