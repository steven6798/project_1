package list;

public class ArrayListFactory<E> implements ListFactory<E>{
	
	/**
	 * Creates a new ArrayList container
	 * @return the newly created container.
	 */
	public ArrayList<E> newInstance() {
		return new ArrayList<E>();
	}

}
