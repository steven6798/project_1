package list;

public interface ListFactory<E> {
	
	/**
	 * Creates a new List container
	 * @return the newly created container.
	 */
	public List<E> newInstance();

}
